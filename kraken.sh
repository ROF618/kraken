#!/usr/bin/env bash

if [ ! -d "./tenticles" ] ; then
    cp -r ~/OffSec/tools/kraken/scripts ./
fi

read -p "Enter target domain " dom
read -p "Enter IP Block " ip_block

amass intel -d $dom -whois -ip -dir ./ -config ~/.config/amass/bravo_config.ini -o intel_$dom.txt &
# this needs to be run after the amass_intel script is run to pull ip addresses

pid=$!

wait $pid

grep -o '[0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}' intel_$dom.txt > ips.txt

wait $pid

if [ -z $ip_block ] ; then
    continue
else
    amass intel -dir ./ -ip -src -active -cidr $ip_block >> intel_$dom.txt
fi

wait $pid

amass enum -active -d $dom -brute -w /home/chris/OffSec/tools/kraken/word_list.txt -dir ./ -config ~/.config/amass/bravo_config.ini -o enum_$dom.txt

wait $pid

# NEED TO IMPLEMENT HAKCRAWLER THAT PIPES A VULN scanner
httpx -l enum_$dom.txt | tee probed_enum_$dom | gospider -S probed_hosts.txt --depth 4 --js --sitemap --robots -c 3 -o crawled_$dom.txt

wait $pid

nuclei -l probed_enum_$dom -t /home/chris/OffSec/tools/nuclei-templates/vulnerabilities/ -o nuclei_results

