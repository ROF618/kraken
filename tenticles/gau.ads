name = "gau"
type = "ext"

function vertical(ctx, domain)
    print("in gau")
    local cmd = outputdir(ctx) .. "gau -subs " .. domain

    local data = asset(io.popen(cmd))
    for line in data:lines() do
        newname(ctx, line)
    end
    data:close()
end
